﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transaction 
    {
        [Key]
        [ForeignKey("User")]

        public int Id { get; set; }  
        public decimal Amount { get; set; }
        
        public string UserToId { get; set; }
        public string UserFromId { get; set; }
        
        public DateTime? TransactionDate { get; set; }
       
        public User UserTo { get; set; }
        public User UserFron { get; set; }

    }
}
