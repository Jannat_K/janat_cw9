﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MoneyTransfer.Models
{
    public class User : IdentityUser
    {
        public string Name { get; set; }
        public decimal Bill { get; set; }
        public int PersonalNumber { get; set; }



        public string GenerateRandomNumber()
        {
            string _numbers = "1234567890";
            Random rand = new Random((int)DateTime.Now.Ticks);
            StringBuilder builder = new StringBuilder(6);
            string numberAsString = "";
            int numberAsNumber = 0;
            for (var i = 0; i < 6; i++)
            {
                builder.Append(_numbers[rand.Next(0, _numbers.Length)]);
            }
            numberAsString = builder.ToString();
            numberAsNumber = int.Parse(numberAsString);
            PersonalNumber = numberAsNumber;
            return (PersonalNumber.ToString());
        }

        public decimal GetBill()
        {    
            Bill = 1000;
            return (Bill);
        }


    }
}
