﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Controllers
{
    public class TransactionsController : Controller
    {
        private readonly ApplicationContext _context;


        public TransactionsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: Transactions
        public async Task<IActionResult> Index()
        {
            IEnumerable<Transaction> transactions =
                 _context.Transactions.Include(u => u.UserFron).Include(u => u.UserTo);

            return View("Index", transactions);

        }


        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transactions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (transaction == null)
            {
                return NotFound();
            }

            return View("Details", transaction);
        }

        // GET: Transactions/Create
        public IActionResult Create()
        {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public decimal Create(Transaction transaction)
        {

            var userTo = _context.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            var userFron = transaction.UserFron;
           

            //var userFron = transaction.UserFron;
            var tr = new Transaction
            {
                UserTo = userTo,
                UserFron = userFron,
                TransactionDate = DateTime.Now
            };

            try
            {
                _context.Transactions.Add(transaction);

                userFron.Bill -= transaction.Amount;

                _context.Users.Attach(userFron);
                _context.Entry(userFron).State = EntityState.Modified;

                userTo.Bill += transaction.Amount;

                _context.Users.Attach(userTo);
                _context.Entry(userTo).State = EntityState.Modified;

                _context.SaveChanges();


                return tr.Id;


            }
            catch (Exeption ex)
            {
                return 0;
            }
        }

    }
}
