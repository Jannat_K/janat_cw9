﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MoneyTransfer.Models;

namespace MoneyTransfer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationContext>();

            services.AddMvc();
            services.AddTransient<IUserClaimsPrincipalFactory<User>, ClaimsPrincipalFactory>();

        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        public class ClaimsPrincipalFactory : UserClaimsPrincipalFactory<User>
        {
            public ClaimsPrincipalFactory(UserManager<User> userManager, IOptions<IdentityOptions> optionsAccessor) : base(userManager, optionsAccessor)
            {
            }

            public override async Task<ClaimsPrincipal> CreateAsync(User user)
            {
                var claims = await base.CreateAsync(user);
                claims.AddIdentity(new ClaimsIdentity(new List<Claim> { new Claim(nameof(user.PersonalNumber), user.PersonalNumber.ToString()) }));
                claims.AddIdentity(new ClaimsIdentity(new List<Claim> { new Claim(nameof(user.Bill), user.Bill.ToString()) }));

                return claims;
            }
        }
    }
}
